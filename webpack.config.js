const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const WebpackShellPlugin = require("webpack-shell-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");

const PATHS = {
  dev: path.join(__dirname, "dev"),
  build: path.join(__dirname, "build"),
};

module.exports = (env, arg) => {
  console.log("arg: ", arg.mode);
  let isDev = arg.mode === "development";

  return {
    entry: {
      main: "./dev/js/main.js",
      vendors: "./dev/js/vendors.js",
    },
    output: {
      path: path.resolve(__dirname, "build/js"),
      filename: "[name].bundle.js",
    },
    resolve: {
      alias: {
        MODE_ENV: arg.mode,
      },
    },
    watch: isDev ? true : false,
    devtool: isDev ? "inline-source-map" : "",
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                // you can specify a publicPath here
                // by default it use publicPath in webpackOptions.output
                publicPath: "../",
              },
            },
            "css-loader",
          ],
        },
        // {
        //   test: /\.js$/,
        //   exclude: /(node_modules)/,
        //   use: {
        //     loader: "babel-loader"
        //   }
        // },
      ],
    },
    plugins: [
      isDev
        ? () => {}
        : new CopyWebpackPlugin({
            patterns: [
              {
                context: PATHS.dev,
                from: "./js/backend/backend.js",
                to: PATHS.build + "/js/backend/backend.js",
              },
              {
                context: PATHS.dev,
                from: "./js/vendors/**/*.*",
                to: PATHS.build,
              },
              {
                context: PATHS.dev,
                from: "./assets/**/*.*", // Copy JSON data files
                to: PATHS.build,
              },
            ],
          }),
      new webpack.DefinePlugin({
        MODE_ENV: JSON.stringify(arg.mode),
      }),
      new WebpackShellPlugin({
        onBuildStart: ['echo "Start building..."'],
        onBuildEnd: [
          `cross-env NODE_ENV=${arg.mode} node server.js ${arg.mode}`,
        ],
      }),
      new MiniCssExtractPlugin({
        filename: "../css/[name].bundle.css",
      }),
    ],
  };
};
