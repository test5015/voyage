let hero = S.first(".hero");
let heroSwiperPrevElm = S.first(hero, ".swiper-button-prev");
let heroSwiperNextElm = S.first(hero, ".swiper-button-next");

console.log(heroSwiperNextElm);

let sw = new Swiper("#swiper-hero", {
  navigation: {
    nextEl: heroSwiperNextElm,
    prevEl: heroSwiperPrevElm,
  },
});

console.log(sw);
