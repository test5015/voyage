#Form

This is form.

##PUG
###+form
```pug
    {
        title: 'This is sample form',
    }
```

###+formGroup
```pug
    {
        type:'text',
        required: true,
        placeholder: null,
        floatingLabel: null,
        inRow: false,
        checked: false,
        elms: null,
        value: null,
        name: null,
        icon: null,
        checkedIndex: 0,
        groupRequired: false,
        inputAttrs: {},
    }
```
####Options
| Variable      | Type             | Default                          | Description  |
| ------------- |:----------------:| --------------------------------:| ------------:|
| type          | String           | text                             | Input `type`         |
| required      | Boolean          | true                             | Add `require` attribute |
| placeholder   | String           | null                             | Add `placeholder` attribute |
| floatingLabel | String           | null                             | Add animated label          |
| inRow         | Boolean          | false                            | Add class `.form__group--row`|
| checked       | Boolean          | false                            | Add `checked` atribute |
| elms          | Array            | [string, string, ...]            | Array of names of checkboxes or radios|
| value         | String            | null                            | Add `value` attribute |
| name          | String            |  null                          |  Add `name` attribute |
| icon         | Object | String   | null                             | String: should be path to icon. Object: params of +icon mixin |
| checkedIndex  | Number            | 0                               | Index of checked radio or checkbox |
| groupRequired | Boolean           | false                           | Add `data-group-required` attribute. Needed to check at least one checked input in group of inputs (radios or checkboxes)   |
| inputAttrs    | Object            | {}                              | Add custom attributes to input (not form__group) |

###+textarea
```pug
    {
        placeholder: null,
        minLength: 2,
        maxLength: null,
        required: false,
        floatingLabel: null,
        counter: null,
        icon: null
    }
```
####Options
| Variable      | Type             | Default                          | Description  |
| ------------- |:----------------:| --------------------------------:| ------------:|
| required      | Boolean          | true                             | Add `require` attribute |
| placeholder   | String           | null                             | Add `placeholder` attribute |
| floatingLabel | String           | null                             | Add animated label          |
| minLength      | Number           | 2                               | Add `data-min` attribute. Works on validation |
| maxLength      | Number           | null                            | Add `data-max` attribute. Works on validation |
| counter       | Number           | null                             | Add counter of max. entered digits |
| icon         | Object | String   | null                             | String: should be path to icon. Object: params of +icon mixin |

###+upload-files
```pug
    {
        icon: {
            name: '#ic-clip', 
            width: 16, 
            height: 16,
            pos: 'left'
        },
        title: 'Прикрепить файл',
        container: true,
        formGroup: {
            type: 'file',
            required: false, 
            inputAttrs: {
                multiple: true
            }
        }
    }
```
####Options
| Variable      | Type             | Default                          | Description  |
| ------------- |:----------------:| --------------------------------:| ------------:|
| icon          | Object           | { ...look above... }             | Add `+icon` mixin |
| title         | String           | Прикрепить файл                  | Button title |
| container     | Boolean          | true                             | Add `ul.uploaded-files` into form          |
| formGroup     | Object           | {...look above...}               | Attach `.form__group` parameters |

####Example
```pug
    +form

    //- or

    +form
        +formGroup({placeholder: 'Sample text', icon: {name: '#ic-polar-bear'}, })
        +formGroup({placeholder: 'Sample email', type:'email', icon: '../../assets/img/ic-polar-bear.svg'})
        +formGroup({floatingLabel: 'Sample phone', type:'number', value: '123'})
        +textarea({floatingLabel: 'Testing', maxLength: 15, required: true})
        +formGroup({type: 'radio', elms: ['Radio 1', 'Radio 2', 'Radio 3'], inRow: true, name: 'radio1', groupRequired: true})
        +formGroup({type: 'checkbox', elms: ['Checkbox 1', 'Checkbox 2', 'Checkbox 3'], inRow: true, groupRequired: true, name: 'check'})
        +upload-files
        +formSuccess
        +btn().js-form-submit
```


