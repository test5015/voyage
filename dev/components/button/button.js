// Disabled buttons


document.addEventListener('click', function (e) {
    if (e.target.closest('.btn--disabled')) {
        e.preventDefault()
        e.stopPropagation()
        return false
    }
})