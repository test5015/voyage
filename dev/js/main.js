// Components
import "../components/button/button";
import "../components/nav/nav";
import "../components/header/header";
import "../components/icon/icon";
import "../components/form/form";
import "../components/picture/picture";
import "../components/socials/socials";
// End: Components

// Pages
import "../pages/index/index";
// End: Pages

// Sections
import "../sections/hero/hero";

import '../sections/footer/footer';
import '../sections/footer/footer';
// End: Sections

let body = S.first("body");

document.addEventListener("navOpen", function () {
  S.addClass(body, "overflow-hidden");
});
document.addEventListener("navClose", function () {
  S.removeClass(body, "overflow-hidden");
});
