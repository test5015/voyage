import Swiper, { Navigation } from "swiper";
import "swiper/swiper.min.css";
Swiper.use([Navigation]);

import throttle from "lodash.throttle";
import debounce from "lodash.debounce";

import flatpickr from "flatpickr";
import "flatpickr/dist/flatpickr.min.css";

window.Swiper = Swiper;
window.throttle = throttle;
window.debounce = debounce;
window.flatpickr = flatpickr;
