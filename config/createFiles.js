const path = require("path");
const fs = require("fs");
const chokidar = require("chokidar");
var autoImport = require('./autoImport')
var includeComponent = require('./includeComponentCall')

const componentTemplate =
    `mixin #{name}(config)
    -
        const defaults = {

        }
    
    -   const params = Object.assign({}, defaults, config)
    
    .#{name}&attributes(attributes)
`;

const sectionTemplate = `
section.#{name}
    
`;

const allPagesTemplate = `        li
            a(href="#{link}.html") #{name}`;

const pageTemplate =
    `extends ../layout.pug
block title
    title #{name}
block main                   
`;

const singleFilePageTemplate =
    `extends ../layout.pug

block main                   
`;

const markdownTemplate =
    `
##{name}

Enter description here...

##PUG
######Defaults
\`\`\`

\`\`\`

####Example
\`\`\`

\`\`\`

**or**
\`\`\`

\`\`\`


####Options
| Variable      | Type             | Default                          | Description  |
| ------------- |:----------------:| --------------------------------:| ------------:|
| name          | type             | value                            | Comment      |
| name          | type             | value                            | Comment      |   
`;

module.exports = (modeSingle) => {
    const watchPaths = ["dev/pages/", "dev/sections/", "dev/components"];
    const templates = {
        component: componentTemplate,
        section: sectionTemplate,
        page: modeSingle ? singleFilePageTemplate : pageTemplate,
        md: markdownTemplate,
        allPages: allPagesTemplate
    };

    initWatchers();

    function initWatchers() {
        const watcher = chokidar.watch(watchPaths, {
            ignored: /(^|[\/\\])\../,
            ignoreInitial: true,
        });

        watcher.on("addDir", filePath => {
            console.log('filePath on add: ' + filePath);
            const types = Object.keys(templates);
            const neededTypes = types.filter(type => {
                return filePath.match(new RegExp(type, "g"));
            });



            const type = neededTypes.length ? neededTypes[0] : false;

            if (type) {
                createNewFile(filePath, type);
            }
        });

        watcher.on("unlinkDir", filePath => {
            const types = Object.keys(templates);
            const neededTypes = types.filter(type => {
                return filePath.match(new RegExp(type, "g"));
            });
            const type = neededTypes.length ? neededTypes[0] : false;
            let index = filePath.split('[\/\\]').indexOf('dev')
            filePath = filePath.split('[\/\\]').splice(index).join('\\')
            let filePathLength = filePath.split('[\/\\]').length
            name = filePath.split('[\/\\]').splice(filePathLength - 1).join('')

            var link = name
            var pageName = name.charAt(0).toUpperCase() + name.slice(1)

            if (type == 'component') {
                includeComponent({
                    delete: true,
                    filePath: filePath
                })
            }

            autoImport({
                delete: true,
                filePath: filePath
            })

            removeData(link, pageName)
        });
    }

    function createNewFile(filePath, type) {
        console.log('type: ' + type);
        const name = getFileName(filePath);
        if (type == 'component') {
            includeComponent({
                name: name,
                filePath: filePath
            })
        }

        autoImport({
            name: name,
            filePath: filePath
        })





        // Exit function if files exists
        if (fs.readdirSync(filePath).length > 0) {
            return
        }

        const content = {
            pug: getPugTemplate(name, type),
            scss: getScssContent(name, type),
            js: getJsContent(name, type),
            md: getMdContent(name, type),
            allPages: getAllPagesTemplate(name, type)
        }

        // if page created - add link in all-pages modal

        if (type == 'page') {
            let file = path.resolve(__dirname, '../dev/pages/all-pages.pug')
            insertData(content, file)
        }

        // Create .pug
        if (!fs.existsSync(path.resolve(filePath) + `/${name}.pug`)) {
            fs.writeFile(path.resolve(filePath) + `/${name}.pug`, content.pug, err => {
                if (err) throw err;
            });
        }

        // Create .md
        if (!fs.existsSync(path.resolve(filePath) + `/${name}.md`) && type === 'component') {
            fs.writeFile(path.resolve(filePath) + `/${name}.md`, content.md, err => {
                if (err) throw err;
            });
        }


        // Create .scss
        if (!fs.existsSync(path.resolve(filePath) + `/${name}.scss`)) {
            fs.writeFile(path.resolve(filePath) + `/${name}.scss`, content.scss, err => {
                if (err) throw err;
            });
        }


        // Create .js
        if (!fs.existsSync(path.resolve(filePath) + `/${name}.js`)) {
            fs.writeFile(path.resolve(filePath) + `/${name}.js`, content.js, err => {
                if (err) throw err;
            });
        }
    }

    function insertData(content, file) {
        let layoutPugFile = file
        let endLine = '        //- End pages List'
        let fileData = fs.readFileSync(layoutPugFile)
            .toString()
            .split("\n")
            .map(elm => {
                return elm.replace('\r', '')
            })
        let lineIndex = fileData.indexOf(endLine);

        let finalData

        fileData.splice(lineIndex, 0, content.allPages)

        finalData = fileData.join('\n')


        fs.writeFile(layoutPugFile, finalData, function (err) {
            if (err) {
                throw err;
            }
        })


    }

    function removeData(link, name) {
        let stringToFind = 'a(href="' + link + '.html") ' + name
        let file = path.resolve(__dirname, '../dev/pages/all-pages.pug')
        let fileData = fs.readFileSync(file).toString().split("\n");

        let finalData;

        fileData.forEach((string, i) => {
            if (string.includes(stringToFind)) {
                fileData.splice(i - 1, 2)
            }
        })

        finalData = fileData.join('\n')

        fs.writeFileSync(file, finalData, function (err) {
            if (err) {
                throw err
            }
        })
    }

    function getAllPagesTemplate(name) {
        const template = allPagesTemplate.replace(/#{link}/, name).replace(/#{name}/, name.charAt(0).toUpperCase() + name.slice(1));

        return template;
    }

    function getPugTemplate(name, type) {
        const template = templates[type].replace(/#{name}/g, name);

        return template;
    }

    function getMdContent(name, type) {
        const template = markdownTemplate.replace(/#{name}/g, name.charAt(0).toUpperCase() + name.slice(1));

        return template;
    }

    function getScssContent(name, type) {
        let content;
        switch (type) {
            case 'component':
            case 'section':
                content = `.${name} {}`;
                break;
            default:
                content = '';
                break;
        }
        return content;
    }

    function getJsContent(name, type) {
        let content;
        switch (type) {
            case 'component':
            case 'section':
            case 'page':
            default:
                content = ``;
                break;
        }
        return content;
    }

    function getFileName(path) {
        return path.replace(/^.*[\\\/]/, "");
    }
}