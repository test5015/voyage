const path = require("path");
const fs = require("fs");

// autoImportComponents()
function includeMixin(options) {
    let filePath = options.filePath || null
    let name = options.name || getFileName(filePath);
    let libPugFile = path.resolve(__dirname, '../dev/pages/lib/lib.pug')
    let libPugEndLine = '                            //- End: Custom components'
    let libPugTemplate = `
                            .lib__heading(id='lib-${name}')
                                h3 ${name}                                
                            .lib__items
                                .lib__item
                                    h4.lib__desc Default
                                    .lib__mixin
                                        +${name}
                                    .lib__actions
                                        +btn({color: 'link', size: 'xs', class: 'code-show'}) Show
                                        +btn({color: 'link', size: 'xs', class: 'code-copy'}) Copy
                                        pre.
                                            +${name}
                            //- End: Custom components
    `



    fs.stat(libPugFile, function(err, stat) {
        if(err == null) {
            if (options.delete) {
                removeFromFile(libPugFile, libPugTemplate, libPugEndLine)
            } else {
                console.log('------ trying to append-----');
                
                appendToFile({
                    file: libPugFile,
                    endLine: libPugEndLine,
                    template: libPugTemplate
                })
            }

        } else if(err.code === 'ENOENT') {
            // file does not exist
            console.log('File not exists ---------');

            // fs.writeFile('log.txt', 'Some log\n');
        } else {
            console.log('Some other error: ', err.code);

        }
    });


}

function appendToFile(obj) {
    let file = obj.file;
    let endLine = obj.endLine
    let template = obj.template

    let finalData = fs.readFileSync(file)
        .toString()
        .replace(endLine, template)
    fs.writeFile(file, finalData, function (err) {
        if (err) {
            throw err;
        }
    })

}

function removeFromFile(file, template, endline) {
    let finalData = fs.readFileSync(file).toString().replace(template, endline);
    fs.writeFileSync(file, finalData, function (err) {
        if (err) {
            throw err
        }
    })
}

function getFileName(path) {
    return path.replace(/^.*[\\\/]/, "");
}




module.exports = includeMixin