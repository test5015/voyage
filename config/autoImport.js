const path = require("path");
const fs = require("fs");

// autoImportComponents()
function autoImportComponents(options) {
    let filePath = options.filePath || null
    let name = options.name || getFileName(filePath);
    let isPage = filePath.includes('pages')
    let isSection = filePath.includes('sections')
    let isComponent = filePath.includes('components')
    let commonJsFile = path.resolve(__dirname, '../dev/js/main.js')
    let commonCssFile = path.resolve(__dirname, '../dev/scss/main.scss')
    let mixinPugFile = path.resolve(__dirname, '../dev/pages/mixins.pug')



    if (isComponent) {
        if (options.delete) {
            removeFromFile(commonJsFile, name, 'components')
            removeFromFile(mixinPugFile, name, 'components')
            removeFromFile(commonCssFile, name, 'components')

        } else {
            appendToFile({
                file: commonJsFile,
                type: 'js',
                endLine: '// End: Components',
                name: name,
                folder: 'components',
            })
            appendToFile({
                file: commonCssFile,
                type: 'scss',
                endLine: '// End: Components',
                name: name,
                folder: 'components',
            })
            appendToFile({
                file: mixinPugFile,
                type: 'pug',
                endLine: '//- End: Mixins',
                name: name,
                folder: 'components',
            })


        }


    } else if (isPage) {
        if (options.delete) {
            removeFromFile(commonJsFile, name, 'pages')
            removeFromFile(commonCssFile, name, 'pages')
        } else {
            appendToFile({
                file: commonJsFile,
                type: 'js',
                endLine: '// End: Pages',
                name: name,
                folder: 'pages',
            })
            appendToFile({
                file: commonCssFile,
                type: 'scss',
                endLine: '// End: Pages',
                name: name,
                folder: 'pages',
            })
        }
    } else {
        if (options.delete) {
            removeFromFile(commonJsFile, name, 'sections')
            removeFromFile(commonCssFile, name, 'sections')
        } else {
            appendToFile({
                file: commonJsFile,
                type: 'js',
                endLine: '// End: Sections',
                name: name,
                folder: 'sections',
            })
            appendToFile({
                file: commonCssFile,
                type: 'scss',
                endLine: '// End: Sections',
                name: name,
                folder: 'sections',
            })
        }
    }
}

function appendToFile(obj) {
    let file = obj.file;
    let name = obj.name;
    let folder = obj.folder;
    let endLine = obj.endLine
    let importKey = obj.type === 'pug' ? 'include' : 'import'

    switch (obj.type) {
        case 'pug':
            importKey = 'include'
            break;
        case 'js':
            importKey = 'import'
            break;
        case 'scss':
            importKey = '@import'
            break;

    }
    let pathAsString = obj.type === 'pug' ? false : true
    let fileData = fs.readFileSync(file)
        .toString()
        .split("\n")
        .map(elm => {
            return elm.replace('\r', '')
        }) // Use map on Windows/Linux to remove carriage return \r
    let lineIndex = fileData.indexOf(endLine);

    let finalData;


    if (pathAsString) {
        // For JS & Scss files
        var ext = ''
        if (obj.type == 'scss') {
            ext = '.scss'
        }
        fileData.splice(lineIndex, 0, `${importKey} \'../${folder}/${name}/${name}${ext}\';`)
    } else {
        // Form Pug files
        fileData.splice(lineIndex, 0, `${importKey} ../${folder}/${name}/${name}`)
    }

    finalData = fileData.join('\n')

    fs.writeFile(file, finalData, function (err) {
        if (err) {
            throw err;
        }
    })

}

function removeFromFile(file, name, folder) {

    let stringToFind = `${folder}/${name}/${name}`

    let commonJsData = fs.readFileSync(file).toString().split("\n");
    let finalData;

    commonJsData.forEach((string, i) => {
        if (string.includes(stringToFind)) {
            commonJsData.splice(i, 1)
        }
    })

    finalData = commonJsData.join('\n')

    fs.writeFileSync(file, finalData, function (err) {
        if (err) {
            throw err
        }
    })

}

function getFileName(path) {
    return path.replace(/^.*[\\\/]/, "");
}




module.exports = autoImportComponents