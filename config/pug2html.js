const pug = require('pug');
const fs = require('fs');
const path = require('path');
const glob = require('glob');


module.exports = function () {
    let promise = new Promise(function (resolve, reject) {
        console.log('pug2Html => Start');
        initPug2Html(resolve)
    })

    return promise;
}

function initPug2Html(resolve) {
    glob('./dev/pages/**/*.pug', {
        ignore: ['./dev/pages/layout.pug', './dev/pages/mixins.pug']
    }, function (err, filePath) {
        filePath.forEach(oneFile => {
            let rendered = pug.renderFile(oneFile, {
                pretty: true
            });
            let result = rendered;
            let oneFielName = getFileName(oneFile)

            result = result.replace(/([\.\/])*\/assets/gi, 'assets')

            fs.writeFile(path.resolve(`build/${oneFielName}.html`), result, 'utf8', function (err) {
                if (err) return console.log(err);
            });
        });

        resolve('pug2html => COMPLETE 🙌');
    })


    function getFileName(filePath) {
        return filePath.replace(/^.*[\\\/]/, '').split('.')[0]
    }
}