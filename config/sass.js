const sass = require('node-sass');
const fs = require('fs');
const mkdirp = require('mkdirp');
const getDirName = require('path').dirname;

const chokidar = require('chokidar')
const postcss = require('postcss')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const fixCssPaths = require('./fixCssPaths')
const isDev = process.env.NODE_ENV !== 'production'

const cssDist = './build/css/'


// Run watcher only for DEV mode
if (isDev) {
    var watcher = chokidar.watch('dev/**/*.scss', {
        ignored: /(^|[\/\\])\../,
        persistent: true
    });

    /**
     * SetTimeout uses to fix a node - sass bug
     * https: //github.com/sass/node-sass/issues/1894
     */
    watcher
        // .on('change', path => renderSass())
        .on('change', path => setTimeout(() => {
            renderSass()
        }, 100))
}


renderSass()

function renderSass() {
    console.time('CSS')
    sass.render({
        file: './dev/scss/main.scss',
        outputStyle: 'expanded',
        sourceMap: isDev ? true : false,
        sourceMapEmbed: isDev ? true : false
    }, function (error, result) { // node-style callback from v3.0.0 onwards
        if (error) {
            console.log(error.status); // used to be "code" in v2x and below
            console.log(error.column);
            console.log(error.message);
            console.log(error.line);
        } else {
            // console.log(result.css);
            // TODO: Поставить тймер для замеров

            if (isDev) {
                mkdirp(getDirName(cssDist + 'main.bundle.css'), function (err) {
                    if (err) {
                        throw err
                    }

                    fs.writeFile(cssDist + 'main.bundle.css', result.css, function (err) {
                        if (!err) {
                            //file written on disk
                        }
                    });
                })
            } else {
                mkdirp(getDirName(cssDist + 'main.bundle.css'), function (err) {
                    if (err) {
                        throw err
                    }

                    writeFileToDist(result)
                })
            }

            console.timeEnd('CSS')
        }
    });


}

function writeFileToDist(result) {
    postcss([autoprefixer, cssnano])
        .process(result.css, {
            from: cssDist + 'main.bundle.css'
        })
        .then(result => {
            // console.log(result);
            fs.writeFile(cssDist + 'main.bundle.css', result.css, () => {
                fixCssPaths()
                return true
            })
            if (result.map) {
                fs.writeFile(cssDist + 'main.bundle.css.map', result.map, () => true)
            }
        })
}