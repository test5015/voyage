const fs = require('fs');
const path = require('path');
const glob = require('glob');


module.exports = function () {
    let promise = new Promise(function (resolve, reject) {

        initFixCssPaths(resolve)

    })

    return promise;
}

function initFixCssPaths(resolve) {
    console.log('fixCssPaths => Start');
    console.time('FixCssPath')
    glob('./build/css/*.css', {
        ignore: ['./dev/pages/layout.pug', './dev/pages/mixins.pug']
    }, function (err, filePath) {
        filePath.forEach(oneFile => {

            let result = fs.readFileSync(oneFile, 'utf8');
            let oneFielName = getFileName(oneFile)

            result = result.replace(/([\.\/])*\/assets/gi, '../assets')

            fs.writeFile(path.resolve(`build/css/${oneFielName}.bundle.css`), result, 'utf8', function (err) {
                if (err) return console.log(err);
            });

        });

        resolve('fixCssPaths => COMPLETE 🙌');
        console.timeEnd('FixCssPath')
    })


    function getFileName(filePath) {
        return filePath.replace(/^.*[\\\/]/, '').split('.')[0]
    }
}