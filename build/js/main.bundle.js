/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./dev/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dev/components/button/button.js":
/*!*****************************************!*\
  !*** ./dev/components/button/button.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Disabled buttons


document.addEventListener('click', function (e) {
    if (e.target.closest('.btn--disabled')) {
        e.preventDefault()
        e.stopPropagation()
        return false
    }
})

/***/ }),

/***/ "./dev/components/form/form.js":
/*!*************************************!*\
  !*** ./dev/components/form/form.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Settings
let formAutofill = true;

// Validation
function Validation(parent, options) {
  var parent = S.find(parent);

  var defaults = {
    formBtn: S.find(parent, ".js-form-submit"),
    preloader: false,
    offset: 0,
    scroll: true,
    speed: 1000,
    checkOnInput: false,
    onComplete: function () {},
  };

  var params = Object.assign({}, defaults, options);

  var emailRegExp =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var required = S.find(parent, "[required]");
  var groupRequired = S.find(parent, "[data-group-required]");
  var result = [];
  var ifSucces = [];
  var succesValue = false;

  /**
   * Live input validation when user cahnge value in input
   * else
   * Check all form with all inputs
   */
  if (params.checkOnInput) {
    required.on("input", checkInput);
  } else {
    checkInputs();
  }

  function checkInputs() {
    // cheking all inputs that have required
    for (var i = 0; i < required.length; i++) {
      var arr = [];

      // cheking empty. Ecception: input[type="file"]
      if (S.attr(required[i], "type") !== "file") {
        arr.push(checkEmpty(required[i]));
      }

      // cheking the Email
      if (S.attr(required[i], "type") === "email") {
        arr.push(checkEmail(required[i]));
      }

      // cheking minimal symbols in string
      if (S.attr(required[i], "data-min")) {
        arr.push(checkMin(required[i]));
      }

      // cheking maximal symbols in string

      if (S.attr(required[i], "data-max")) {
        arr.push(checkMax(required[i]));
      }

      // cheking if checkbox is cheked
      if (S.attr(required[i], "type") == "checkbox") {
        arr.push(checkCheckbox(required[i]));
      }

      // One lowercase letter
      if (S.attr(required[i], "data-lowercase") != null) {
        arr.push(checkLowercase(required[i]));
      }

      // One uppercase letter
      if (S.attr(required[i], "data-uppercase") != null) {
        arr.push(checkUppercase(required[i]));
      }

      // One number exist
      if (S.attr(required[i], "data-number") != null) {
        arr.push(checkNumberExist(required[i]));
      }

      // Only digits
      if (S.attr(required[i], "data-digits") != null) {
        arr.push(checkDigits(required[i]));
      }

      // Compare with equal input
      if (S.attr(required[i], "data-equal") != null) {
        let compareValue = S.val(S.attr(required[i], "data-equal"));
        arr.push(checkEqual(required[i], compareValue));
      }

      // Compare input with files
      if (S.attr(required[i], "type") === "file") {
        arr.push(checkFileExist(required[i]));
      }

      // adding object with the containing results
      result.push({
        elm: required[i],
        value: arr.indexOf(false) == -1 ? true : false,
      });

      // adding in mass to check if all inputs pass validation
      ifSucces.push(result[i].value);

      // adding error class to inputs that didnt pass valid
      let formGroup = S.closest(required[i], ".form__group");
      if (arr.indexOf(false) == -1) {
        S.removeClass(formGroup, "error");
      } else {
        S.addClass(formGroup, "error");
      }
    }

    // Check group of checboxes or radios
    if (groupRequired.length > 0) {
      groupRequired.forEach((groupParent) => {
        ifSucces.push(checkGroup(groupParent));
      });
    }

    params.onComplete();

    // Scroll to error
    if (params.scroll && window.innerWidth < 768) {
      scrollOnError();
    }

    succesValue = ifSucces.indexOf(false) == -1 ? true : false;
  }

  function checkInput() {
    var arr = [];
    var errors = {};
    arr.push("empty: " + checkEmpty(this));
    errors.empty = checkEmpty(this);

    // cheking the Email
    if (S.attr(this, "type") === "email") {
      arr.push(checkEmail(this));
      errors.email = checkEmail(this);
    }

    // cheking minimal symbols in string

    if (S.attr(this, "data-min")) {
      arr.push("min: " + checkMin(this));
      errors.min = checkMin(this);
    }

    // cheking maximal symbols in string
    if (S.attr(this, "data-max")) {
      arr.push(checkMax(this));
      errors.max = checkMax(this);
    }

    // One lowercase letter
    if (S.attr(this, "data-lowercase") != null) {
      arr.push("lowercase: " + checkLowercase(this));
      errors.lowercase = checkLowercase(this);
    }

    // One uppercase letter
    if (S.attr(this, "data-uppercase") != null) {
      arr.push("uppercase: " + checkUppercase(this));
      errors.uppercase = checkUppercase(this);
    }

    // One number exist
    if (S.attr(this, "data-number") != null) {
      arr.push("number: " + checkNumberExist(this));
      errors.number = checkNumberExist(this);
    }

    // Digits only
    if (S.attr(this, "data-digits") != null) {
      arr.push("digits: " + checkDigits(this));
      errors.number = checkDigits(this);
    }

    // Compare with old password
    if (S.attr(this, "data-compare") != null) {
      let compareValue = S.val(S.attr(this, "data-compare"));
      arr.push("compare: " + checkCompare(this, compareValue));
      errors.compare = checkCompare(this, compareValue);
    }

    // Compare with equal input
    if (S.attr(this, "data-equal") != null) {
      let compareValue = S.val(S.attr(this, "data-equal"));
      arr.push("equal: " + checkEqual(this, compareValue));
    }

    // adding object with the containing results
    result.push({
      elm: this,
      value: arr.indexOf(false) == -1 ? true : false,
    });

    // adding in mass to check if all inputs pass validation
    ifSucces.push(result.value);

    // adding error class to inputs that didnt pass valid
    let formGroup = S.closest(this, ".form__group");
    if (arr.indexOf(false) == -1) {
      S.removeClass(formGroup, "error");
    } else {
      S.addClass(formGroup, "error");
    }

    params.onComplete(errors);
  }

  function setSuccess() {
    S.addClass(parent, "success");

    setTimeout(function () {
      S.removeClass(parent, "success");
    }, 2000);

    stopPreload();
    clearInputs();
  }

  function clearInputs() {
    parent[0].reset();
    S.removeClass(S.find(parent, ".active"), "active");

    if (uploadForm) {
      uploadForm.clearAllFiles();
    }
  }

  function scrollOnError() {
    let firstError = S.find(parent, ".error")[0];

    if (!firstError) {
      return;
    }

    // Add focus to first input element
    setTimeout(() => {
      S.find(firstError, "input")[0].focus();
    }, 500);

    // Scroll to first error
    S.scrollTo(window, {
      top: firstError,
      offsetY: 100,
    });
  }

  // validation functions
  function checkEmpty(elem) {
    var str = S.val(elem);

    str.replace(/^\s\s*/, "").replace(/\s\s*$/, "");

    if (S.val(elem).length <= 0) {
      return false;
    }
    return true;
  }

  function checkEmail(elem) {
    var text = S.val(elem);
    if (emailRegExp.test(text)) {
      return true;
    }
    return false;
  }

  function checkMin(elem) {
    if (S.val(elem).length <= S.attr(elem, "data-min") - 1) {
      return false;
    }
    return true;
  }

  function checkMax(elem) {
    if (S.val(elem).length > S.attr(elem, "data-max")) {
      return false;
    }
    return true;
  }

  function checkLowercase(elem) {
    return /[a-z]/.test(S.val(elem));
  }

  function checkUppercase(elem) {
    return /[A-Z]/.test(S.val(elem));
  }

  function checkNumberExist(elem) {
    return /[0-9]/.test(S.val(elem));
  }

  function checkDigits(elem) {
    return /^\d+$/.test(S.val(elem));
  }

  function checkCompare(elem, val) {
    return S.val(elem) !== val;
  }

  function checkEqual(elem, val) {
    return S.val(elem) === val;
  }

  function checkCheckbox(elem) {
    if (S.prop(elem, "checked")) {
      return true;
    }

    return false;
  }

  function checkGroup(group) {
    let inputs = S.find(group, 'input[type="checkbox"], input[type="radio"]');
    let results = [];

    S.each(inputs, function () {
      results.push(S.prop(this, "checked"));
    });

    if (results.indexOf(true) == -1) {
      S.addClass(group, "error");
    } else {
      S.removeClass(group, "error");
    }

    return results.indexOf(true) != -1;
  }

  function checkFileExist(elem) {
    if (!elem.filesArr) {
      return false;
    }

    return elem.filesArr.length > 0;
  }

  function startPreload() {
    S.addClass(params.formBtn, "btn--preload").attr(
      params.formBtn,
      "disabled",
      true
    );
    S.addClass(parent, "form--preload");
  }

  function stopPreload() {
    S.removeClass(params.formBtn, "btn--preload").attr(
      params.formBtn,
      "disabled",
      false
    );
    S.removeClass(parent, "form--preload");
  }

  return {
    isValid: succesValue,
    clearInputs: clearInputs,
    startPreload: startPreload,
    stopPreload: stopPreload,
    setSuccess: setSuccess,
  };
}

// Autofill form
if (formAutofill) {
  S.on(".form input", "keyup", function (e) {
    if (e.altKey == true && e.keyCode == 86) {
      let thisForm = S.closest(this, ".form");
      let textInps = S.find(thisForm, 'input[type="text"]');
      let passInps = S.find(thisForm, 'input[type="password"]');
      let numberInps = S.find(
        thisForm,
        'input[type="number"], input[type="tel"]'
      );
      let emailInps = S.find(thisForm, 'input[type="email"]');
      let textarea = S.find(thisForm, "textarea");
      S.addClass(this, "active");
      S.addClass(textInps, "active");
      S.addClass(passInps, "active");
      S.addClass(numberInps, "active");
      S.addClass(emailInps, "active");
      S.addClass(textarea, "active");
      S.val(textInps, "Auto filled input");
      S.val(passInps, "autofill");
      S.val(numberInps, "123456789");
      S.val(emailInps, "autofill@form.com");
      S.val(
        textarea,
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultricies lobortis felis, eu fermentum tortor aliquet eu. Nunc neque nibh, accumsan non placerat at, imperdiet nec libero. Proin nec nulla et risus accumsan viverra sed dapibus diam. Ut vel mauris commodo, posuere tortor id, pharetra libero. Nulla faucibus sem eu massa malesuada aliquet. Integer ultrices libero venenatis elit egestas aliquam. Morbi viverra nisl eros, vitae eleifend enim vulputate ac. Vestibulum venenatis eu lacus et ultricies. In hac habitasse platea dictumst. Etiam justo ipsum, viverra quis pretium a, gravida quis metus."
      );
    }
  });
}

if (!window.App) {
  window.App = {};
  window.App.Validation = Validation;
} else {
  window.App.Validation = Validation;
}


/***/ }),

/***/ "./dev/components/header/header.js":
/*!*****************************************!*\
  !*** ./dev/components/header/header.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./dev/components/icon/icon.js":
/*!*************************************!*\
  !*** ./dev/components/icon/icon.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./dev/components/nav/nav.js":
/*!***********************************!*\
  !*** ./dev/components/nav/nav.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// JS

(function () {
    let nav = document.querySelector('#nav');
    if (!nav) return // Exit if #nav doesn't exist

    let toggleBtn = nav.parentNode.querySelector('#nav__toggle');

    let navBG = nav.querySelector('.nav__bg')
    let navOpen = new CustomEvent('navOpen')
    let navClose = new CustomEvent('navClose')
    let isOpen = false;


    toggleBtn.addEventListener('click', toggleNav);
    navBG.addEventListener('click', toggleNav);

    function toggleNav() {
        if (isOpen) {
            document.dispatchEvent(navClose)
            nav.classList.remove('open')
            toggleBtn.classList.remove('open')
        } else {
            document.dispatchEvent(navOpen)
            nav.classList.add('open')
            toggleBtn.classList.add('open')
        }

        isOpen = !isOpen
    }


})()

/***/ }),

/***/ "./dev/components/picture/picture.js":
/*!*******************************************!*\
  !*** ./dev/components/picture/picture.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./dev/components/socials/socials.js":
/*!*******************************************!*\
  !*** ./dev/components/socials/socials.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./dev/js/main.js":
/*!************************!*\
  !*** ./dev/js/main.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_button_button__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/button/button */ "./dev/components/button/button.js");
/* harmony import */ var _components_button_button__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_components_button_button__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_nav_nav__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/nav/nav */ "./dev/components/nav/nav.js");
/* harmony import */ var _components_nav_nav__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_nav_nav__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_header_header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/header/header */ "./dev/components/header/header.js");
/* harmony import */ var _components_header_header__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_header_header__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_icon_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/icon/icon */ "./dev/components/icon/icon.js");
/* harmony import */ var _components_icon_icon__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_icon_icon__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_form_form__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/form/form */ "./dev/components/form/form.js");
/* harmony import */ var _components_form_form__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_form_form__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_picture_picture__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/picture/picture */ "./dev/components/picture/picture.js");
/* harmony import */ var _components_picture_picture__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components_picture_picture__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_socials_socials__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/socials/socials */ "./dev/components/socials/socials.js");
/* harmony import */ var _components_socials_socials__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_socials_socials__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _pages_index_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pages/index/index */ "./dev/pages/index/index.js");
/* harmony import */ var _pages_index_index__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_pages_index_index__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _sections_hero_hero__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../sections/hero/hero */ "./dev/sections/hero/hero.js");
/* harmony import */ var _sections_hero_hero__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_sections_hero_hero__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _sections_footer_footer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../sections/footer/footer */ "./dev/sections/footer/footer.js");
/* harmony import */ var _sections_footer_footer__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_sections_footer_footer__WEBPACK_IMPORTED_MODULE_9__);
// Components







// End: Components

// Pages

// End: Pages

// Sections




// End: Sections

let body = S.first("body");

document.addEventListener("navOpen", function () {
  S.addClass(body, "overflow-hidden");
});
document.addEventListener("navClose", function () {
  S.removeClass(body, "overflow-hidden");
});


/***/ }),

/***/ "./dev/pages/index/index.js":
/*!**********************************!*\
  !*** ./dev/pages/index/index.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

let hero = S.first(".hero");
let heroSwiperPrevElm = S.first(hero, ".swiper-button-prev");
let heroSwiperNextElm = S.first(hero, ".swiper-button-next");

console.log(heroSwiperNextElm);

let sw = new Swiper("#swiper-hero", {
  navigation: {
    nextEl: heroSwiperNextElm,
    prevEl: heroSwiperPrevElm,
  },
});

console.log(sw);


/***/ }),

/***/ "./dev/sections/footer/footer.js":
/*!***************************************!*\
  !*** ./dev/sections/footer/footer.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./dev/sections/hero/hero.js":
/*!***********************************!*\
  !*** ./dev/sections/hero/hero.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vZGV2L2NvbXBvbmVudHMvYnV0dG9uL2J1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9kZXYvY29tcG9uZW50cy9mb3JtL2Zvcm0uanMiLCJ3ZWJwYWNrOi8vLy4vZGV2L2NvbXBvbmVudHMvbmF2L25hdi5qcyIsIndlYnBhY2s6Ly8vLi9kZXYvanMvbWFpbi5qcyIsIndlYnBhY2s6Ly8vLi9kZXYvcGFnZXMvaW5kZXgvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLEM7Ozs7Ozs7Ozs7O0FDVEQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEI7QUFDOUI7O0FBRUEsK0JBQStCOztBQUUvQjtBQUNBLHNCQUFzQix5QkFBeUIsZ0NBQWdDLElBQUksUUFBUSxJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksaUNBQWlDLEdBQUc7QUFDM0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFBbUIscUJBQXFCO0FBQ3hDOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFaQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQSxDQUFDLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQ0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDcUM7QUFDTjtBQUNNO0FBQ0o7QUFDQTtBQUNNO0FBQ0E7QUFDdkM7O0FBRUE7QUFDOEI7QUFDOUI7O0FBRUE7QUFDK0I7O0FBRUk7QUFDQTtBQUNuQzs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7QUM1QkQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQiLCJmaWxlIjoibWFpbi5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2Rldi9qcy9tYWluLmpzXCIpO1xuIiwiLy8gRGlzYWJsZWQgYnV0dG9uc1xuXG5cbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICBpZiAoZS50YXJnZXQuY2xvc2VzdCgnLmJ0bi0tZGlzYWJsZWQnKSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG59KSIsIi8vIFNldHRpbmdzXG5sZXQgZm9ybUF1dG9maWxsID0gdHJ1ZTtcblxuLy8gVmFsaWRhdGlvblxuZnVuY3Rpb24gVmFsaWRhdGlvbihwYXJlbnQsIG9wdGlvbnMpIHtcbiAgdmFyIHBhcmVudCA9IFMuZmluZChwYXJlbnQpO1xuXG4gIHZhciBkZWZhdWx0cyA9IHtcbiAgICBmb3JtQnRuOiBTLmZpbmQocGFyZW50LCBcIi5qcy1mb3JtLXN1Ym1pdFwiKSxcbiAgICBwcmVsb2FkZXI6IGZhbHNlLFxuICAgIG9mZnNldDogMCxcbiAgICBzY3JvbGw6IHRydWUsXG4gICAgc3BlZWQ6IDEwMDAsXG4gICAgY2hlY2tPbklucHV0OiBmYWxzZSxcbiAgICBvbkNvbXBsZXRlOiBmdW5jdGlvbiAoKSB7fSxcbiAgfTtcblxuICB2YXIgcGFyYW1zID0gT2JqZWN0LmFzc2lnbih7fSwgZGVmYXVsdHMsIG9wdGlvbnMpO1xuXG4gIHZhciBlbWFpbFJlZ0V4cCA9XG4gICAgL14oKFtePD4oKVtcXF1cXFxcLiw7Olxcc0BcXFwiXSsoXFwuW148PigpW1xcXVxcXFwuLDs6XFxzQFxcXCJdKykqKXwoXFxcIi4rXFxcIikpQCgoXFxbWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcXSl8KChbYS16QS1aXFwtMC05XStcXC4pK1thLXpBLVpdezIsfSkpJC87XG4gIHZhciByZXF1aXJlZCA9IFMuZmluZChwYXJlbnQsIFwiW3JlcXVpcmVkXVwiKTtcbiAgdmFyIGdyb3VwUmVxdWlyZWQgPSBTLmZpbmQocGFyZW50LCBcIltkYXRhLWdyb3VwLXJlcXVpcmVkXVwiKTtcbiAgdmFyIHJlc3VsdCA9IFtdO1xuICB2YXIgaWZTdWNjZXMgPSBbXTtcbiAgdmFyIHN1Y2Nlc1ZhbHVlID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIExpdmUgaW5wdXQgdmFsaWRhdGlvbiB3aGVuIHVzZXIgY2FobmdlIHZhbHVlIGluIGlucHV0XG4gICAqIGVsc2VcbiAgICogQ2hlY2sgYWxsIGZvcm0gd2l0aCBhbGwgaW5wdXRzXG4gICAqL1xuICBpZiAocGFyYW1zLmNoZWNrT25JbnB1dCkge1xuICAgIHJlcXVpcmVkLm9uKFwiaW5wdXRcIiwgY2hlY2tJbnB1dCk7XG4gIH0gZWxzZSB7XG4gICAgY2hlY2tJbnB1dHMoKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNoZWNrSW5wdXRzKCkge1xuICAgIC8vIGNoZWtpbmcgYWxsIGlucHV0cyB0aGF0IGhhdmUgcmVxdWlyZWRcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJlcXVpcmVkLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgYXJyID0gW107XG5cbiAgICAgIC8vIGNoZWtpbmcgZW1wdHkuIEVjY2VwdGlvbjogaW5wdXRbdHlwZT1cImZpbGVcIl1cbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwidHlwZVwiKSAhPT0gXCJmaWxlXCIpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tFbXB0eShyZXF1aXJlZFtpXSkpO1xuICAgICAgfVxuXG4gICAgICAvLyBjaGVraW5nIHRoZSBFbWFpbFxuICAgICAgaWYgKFMuYXR0cihyZXF1aXJlZFtpXSwgXCJ0eXBlXCIpID09PSBcImVtYWlsXCIpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tFbWFpbChyZXF1aXJlZFtpXSkpO1xuICAgICAgfVxuXG4gICAgICAvLyBjaGVraW5nIG1pbmltYWwgc3ltYm9scyBpbiBzdHJpbmdcbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwiZGF0YS1taW5cIikpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tNaW4ocmVxdWlyZWRbaV0pKTtcbiAgICAgIH1cblxuICAgICAgLy8gY2hla2luZyBtYXhpbWFsIHN5bWJvbHMgaW4gc3RyaW5nXG5cbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwiZGF0YS1tYXhcIikpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tNYXgocmVxdWlyZWRbaV0pKTtcbiAgICAgIH1cblxuICAgICAgLy8gY2hla2luZyBpZiBjaGVja2JveCBpcyBjaGVrZWRcbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwidHlwZVwiKSA9PSBcImNoZWNrYm94XCIpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tDaGVja2JveChyZXF1aXJlZFtpXSkpO1xuICAgICAgfVxuXG4gICAgICAvLyBPbmUgbG93ZXJjYXNlIGxldHRlclxuICAgICAgaWYgKFMuYXR0cihyZXF1aXJlZFtpXSwgXCJkYXRhLWxvd2VyY2FzZVwiKSAhPSBudWxsKSB7XG4gICAgICAgIGFyci5wdXNoKGNoZWNrTG93ZXJjYXNlKHJlcXVpcmVkW2ldKSk7XG4gICAgICB9XG5cbiAgICAgIC8vIE9uZSB1cHBlcmNhc2UgbGV0dGVyXG4gICAgICBpZiAoUy5hdHRyKHJlcXVpcmVkW2ldLCBcImRhdGEtdXBwZXJjYXNlXCIpICE9IG51bGwpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tVcHBlcmNhc2UocmVxdWlyZWRbaV0pKTtcbiAgICAgIH1cblxuICAgICAgLy8gT25lIG51bWJlciBleGlzdFxuICAgICAgaWYgKFMuYXR0cihyZXF1aXJlZFtpXSwgXCJkYXRhLW51bWJlclwiKSAhPSBudWxsKSB7XG4gICAgICAgIGFyci5wdXNoKGNoZWNrTnVtYmVyRXhpc3QocmVxdWlyZWRbaV0pKTtcbiAgICAgIH1cblxuICAgICAgLy8gT25seSBkaWdpdHNcbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwiZGF0YS1kaWdpdHNcIikgIT0gbnVsbCkge1xuICAgICAgICBhcnIucHVzaChjaGVja0RpZ2l0cyhyZXF1aXJlZFtpXSkpO1xuICAgICAgfVxuXG4gICAgICAvLyBDb21wYXJlIHdpdGggZXF1YWwgaW5wdXRcbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwiZGF0YS1lcXVhbFwiKSAhPSBudWxsKSB7XG4gICAgICAgIGxldCBjb21wYXJlVmFsdWUgPSBTLnZhbChTLmF0dHIocmVxdWlyZWRbaV0sIFwiZGF0YS1lcXVhbFwiKSk7XG4gICAgICAgIGFyci5wdXNoKGNoZWNrRXF1YWwocmVxdWlyZWRbaV0sIGNvbXBhcmVWYWx1ZSkpO1xuICAgICAgfVxuXG4gICAgICAvLyBDb21wYXJlIGlucHV0IHdpdGggZmlsZXNcbiAgICAgIGlmIChTLmF0dHIocmVxdWlyZWRbaV0sIFwidHlwZVwiKSA9PT0gXCJmaWxlXCIpIHtcbiAgICAgICAgYXJyLnB1c2goY2hlY2tGaWxlRXhpc3QocmVxdWlyZWRbaV0pKTtcbiAgICAgIH1cblxuICAgICAgLy8gYWRkaW5nIG9iamVjdCB3aXRoIHRoZSBjb250YWluaW5nIHJlc3VsdHNcbiAgICAgIHJlc3VsdC5wdXNoKHtcbiAgICAgICAgZWxtOiByZXF1aXJlZFtpXSxcbiAgICAgICAgdmFsdWU6IGFyci5pbmRleE9mKGZhbHNlKSA9PSAtMSA/IHRydWUgOiBmYWxzZSxcbiAgICAgIH0pO1xuXG4gICAgICAvLyBhZGRpbmcgaW4gbWFzcyB0byBjaGVjayBpZiBhbGwgaW5wdXRzIHBhc3MgdmFsaWRhdGlvblxuICAgICAgaWZTdWNjZXMucHVzaChyZXN1bHRbaV0udmFsdWUpO1xuXG4gICAgICAvLyBhZGRpbmcgZXJyb3IgY2xhc3MgdG8gaW5wdXRzIHRoYXQgZGlkbnQgcGFzcyB2YWxpZFxuICAgICAgbGV0IGZvcm1Hcm91cCA9IFMuY2xvc2VzdChyZXF1aXJlZFtpXSwgXCIuZm9ybV9fZ3JvdXBcIik7XG4gICAgICBpZiAoYXJyLmluZGV4T2YoZmFsc2UpID09IC0xKSB7XG4gICAgICAgIFMucmVtb3ZlQ2xhc3MoZm9ybUdyb3VwLCBcImVycm9yXCIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgUy5hZGRDbGFzcyhmb3JtR3JvdXAsIFwiZXJyb3JcIik7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gQ2hlY2sgZ3JvdXAgb2YgY2hlY2JveGVzIG9yIHJhZGlvc1xuICAgIGlmIChncm91cFJlcXVpcmVkLmxlbmd0aCA+IDApIHtcbiAgICAgIGdyb3VwUmVxdWlyZWQuZm9yRWFjaCgoZ3JvdXBQYXJlbnQpID0+IHtcbiAgICAgICAgaWZTdWNjZXMucHVzaChjaGVja0dyb3VwKGdyb3VwUGFyZW50KSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBwYXJhbXMub25Db21wbGV0ZSgpO1xuXG4gICAgLy8gU2Nyb2xsIHRvIGVycm9yXG4gICAgaWYgKHBhcmFtcy5zY3JvbGwgJiYgd2luZG93LmlubmVyV2lkdGggPCA3NjgpIHtcbiAgICAgIHNjcm9sbE9uRXJyb3IoKTtcbiAgICB9XG5cbiAgICBzdWNjZXNWYWx1ZSA9IGlmU3VjY2VzLmluZGV4T2YoZmFsc2UpID09IC0xID8gdHJ1ZSA6IGZhbHNlO1xuICB9XG5cbiAgZnVuY3Rpb24gY2hlY2tJbnB1dCgpIHtcbiAgICB2YXIgYXJyID0gW107XG4gICAgdmFyIGVycm9ycyA9IHt9O1xuICAgIGFyci5wdXNoKFwiZW1wdHk6IFwiICsgY2hlY2tFbXB0eSh0aGlzKSk7XG4gICAgZXJyb3JzLmVtcHR5ID0gY2hlY2tFbXB0eSh0aGlzKTtcblxuICAgIC8vIGNoZWtpbmcgdGhlIEVtYWlsXG4gICAgaWYgKFMuYXR0cih0aGlzLCBcInR5cGVcIikgPT09IFwiZW1haWxcIikge1xuICAgICAgYXJyLnB1c2goY2hlY2tFbWFpbCh0aGlzKSk7XG4gICAgICBlcnJvcnMuZW1haWwgPSBjaGVja0VtYWlsKHRoaXMpO1xuICAgIH1cblxuICAgIC8vIGNoZWtpbmcgbWluaW1hbCBzeW1ib2xzIGluIHN0cmluZ1xuXG4gICAgaWYgKFMuYXR0cih0aGlzLCBcImRhdGEtbWluXCIpKSB7XG4gICAgICBhcnIucHVzaChcIm1pbjogXCIgKyBjaGVja01pbih0aGlzKSk7XG4gICAgICBlcnJvcnMubWluID0gY2hlY2tNaW4odGhpcyk7XG4gICAgfVxuXG4gICAgLy8gY2hla2luZyBtYXhpbWFsIHN5bWJvbHMgaW4gc3RyaW5nXG4gICAgaWYgKFMuYXR0cih0aGlzLCBcImRhdGEtbWF4XCIpKSB7XG4gICAgICBhcnIucHVzaChjaGVja01heCh0aGlzKSk7XG4gICAgICBlcnJvcnMubWF4ID0gY2hlY2tNYXgodGhpcyk7XG4gICAgfVxuXG4gICAgLy8gT25lIGxvd2VyY2FzZSBsZXR0ZXJcbiAgICBpZiAoUy5hdHRyKHRoaXMsIFwiZGF0YS1sb3dlcmNhc2VcIikgIT0gbnVsbCkge1xuICAgICAgYXJyLnB1c2goXCJsb3dlcmNhc2U6IFwiICsgY2hlY2tMb3dlcmNhc2UodGhpcykpO1xuICAgICAgZXJyb3JzLmxvd2VyY2FzZSA9IGNoZWNrTG93ZXJjYXNlKHRoaXMpO1xuICAgIH1cblxuICAgIC8vIE9uZSB1cHBlcmNhc2UgbGV0dGVyXG4gICAgaWYgKFMuYXR0cih0aGlzLCBcImRhdGEtdXBwZXJjYXNlXCIpICE9IG51bGwpIHtcbiAgICAgIGFyci5wdXNoKFwidXBwZXJjYXNlOiBcIiArIGNoZWNrVXBwZXJjYXNlKHRoaXMpKTtcbiAgICAgIGVycm9ycy51cHBlcmNhc2UgPSBjaGVja1VwcGVyY2FzZSh0aGlzKTtcbiAgICB9XG5cbiAgICAvLyBPbmUgbnVtYmVyIGV4aXN0XG4gICAgaWYgKFMuYXR0cih0aGlzLCBcImRhdGEtbnVtYmVyXCIpICE9IG51bGwpIHtcbiAgICAgIGFyci5wdXNoKFwibnVtYmVyOiBcIiArIGNoZWNrTnVtYmVyRXhpc3QodGhpcykpO1xuICAgICAgZXJyb3JzLm51bWJlciA9IGNoZWNrTnVtYmVyRXhpc3QodGhpcyk7XG4gICAgfVxuXG4gICAgLy8gRGlnaXRzIG9ubHlcbiAgICBpZiAoUy5hdHRyKHRoaXMsIFwiZGF0YS1kaWdpdHNcIikgIT0gbnVsbCkge1xuICAgICAgYXJyLnB1c2goXCJkaWdpdHM6IFwiICsgY2hlY2tEaWdpdHModGhpcykpO1xuICAgICAgZXJyb3JzLm51bWJlciA9IGNoZWNrRGlnaXRzKHRoaXMpO1xuICAgIH1cblxuICAgIC8vIENvbXBhcmUgd2l0aCBvbGQgcGFzc3dvcmRcbiAgICBpZiAoUy5hdHRyKHRoaXMsIFwiZGF0YS1jb21wYXJlXCIpICE9IG51bGwpIHtcbiAgICAgIGxldCBjb21wYXJlVmFsdWUgPSBTLnZhbChTLmF0dHIodGhpcywgXCJkYXRhLWNvbXBhcmVcIikpO1xuICAgICAgYXJyLnB1c2goXCJjb21wYXJlOiBcIiArIGNoZWNrQ29tcGFyZSh0aGlzLCBjb21wYXJlVmFsdWUpKTtcbiAgICAgIGVycm9ycy5jb21wYXJlID0gY2hlY2tDb21wYXJlKHRoaXMsIGNvbXBhcmVWYWx1ZSk7XG4gICAgfVxuXG4gICAgLy8gQ29tcGFyZSB3aXRoIGVxdWFsIGlucHV0XG4gICAgaWYgKFMuYXR0cih0aGlzLCBcImRhdGEtZXF1YWxcIikgIT0gbnVsbCkge1xuICAgICAgbGV0IGNvbXBhcmVWYWx1ZSA9IFMudmFsKFMuYXR0cih0aGlzLCBcImRhdGEtZXF1YWxcIikpO1xuICAgICAgYXJyLnB1c2goXCJlcXVhbDogXCIgKyBjaGVja0VxdWFsKHRoaXMsIGNvbXBhcmVWYWx1ZSkpO1xuICAgIH1cblxuICAgIC8vIGFkZGluZyBvYmplY3Qgd2l0aCB0aGUgY29udGFpbmluZyByZXN1bHRzXG4gICAgcmVzdWx0LnB1c2goe1xuICAgICAgZWxtOiB0aGlzLFxuICAgICAgdmFsdWU6IGFyci5pbmRleE9mKGZhbHNlKSA9PSAtMSA/IHRydWUgOiBmYWxzZSxcbiAgICB9KTtcblxuICAgIC8vIGFkZGluZyBpbiBtYXNzIHRvIGNoZWNrIGlmIGFsbCBpbnB1dHMgcGFzcyB2YWxpZGF0aW9uXG4gICAgaWZTdWNjZXMucHVzaChyZXN1bHQudmFsdWUpO1xuXG4gICAgLy8gYWRkaW5nIGVycm9yIGNsYXNzIHRvIGlucHV0cyB0aGF0IGRpZG50IHBhc3MgdmFsaWRcbiAgICBsZXQgZm9ybUdyb3VwID0gUy5jbG9zZXN0KHRoaXMsIFwiLmZvcm1fX2dyb3VwXCIpO1xuICAgIGlmIChhcnIuaW5kZXhPZihmYWxzZSkgPT0gLTEpIHtcbiAgICAgIFMucmVtb3ZlQ2xhc3MoZm9ybUdyb3VwLCBcImVycm9yXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBTLmFkZENsYXNzKGZvcm1Hcm91cCwgXCJlcnJvclwiKTtcbiAgICB9XG5cbiAgICBwYXJhbXMub25Db21wbGV0ZShlcnJvcnMpO1xuICB9XG5cbiAgZnVuY3Rpb24gc2V0U3VjY2VzcygpIHtcbiAgICBTLmFkZENsYXNzKHBhcmVudCwgXCJzdWNjZXNzXCIpO1xuXG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICBTLnJlbW92ZUNsYXNzKHBhcmVudCwgXCJzdWNjZXNzXCIpO1xuICAgIH0sIDIwMDApO1xuXG4gICAgc3RvcFByZWxvYWQoKTtcbiAgICBjbGVhcklucHV0cygpO1xuICB9XG5cbiAgZnVuY3Rpb24gY2xlYXJJbnB1dHMoKSB7XG4gICAgcGFyZW50WzBdLnJlc2V0KCk7XG4gICAgUy5yZW1vdmVDbGFzcyhTLmZpbmQocGFyZW50LCBcIi5hY3RpdmVcIiksIFwiYWN0aXZlXCIpO1xuXG4gICAgaWYgKHVwbG9hZEZvcm0pIHtcbiAgICAgIHVwbG9hZEZvcm0uY2xlYXJBbGxGaWxlcygpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHNjcm9sbE9uRXJyb3IoKSB7XG4gICAgbGV0IGZpcnN0RXJyb3IgPSBTLmZpbmQocGFyZW50LCBcIi5lcnJvclwiKVswXTtcblxuICAgIGlmICghZmlyc3RFcnJvcikge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIEFkZCBmb2N1cyB0byBmaXJzdCBpbnB1dCBlbGVtZW50XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBTLmZpbmQoZmlyc3RFcnJvciwgXCJpbnB1dFwiKVswXS5mb2N1cygpO1xuICAgIH0sIDUwMCk7XG5cbiAgICAvLyBTY3JvbGwgdG8gZmlyc3QgZXJyb3JcbiAgICBTLnNjcm9sbFRvKHdpbmRvdywge1xuICAgICAgdG9wOiBmaXJzdEVycm9yLFxuICAgICAgb2Zmc2V0WTogMTAwLFxuICAgIH0pO1xuICB9XG5cbiAgLy8gdmFsaWRhdGlvbiBmdW5jdGlvbnNcbiAgZnVuY3Rpb24gY2hlY2tFbXB0eShlbGVtKSB7XG4gICAgdmFyIHN0ciA9IFMudmFsKGVsZW0pO1xuXG4gICAgc3RyLnJlcGxhY2UoL15cXHNcXHMqLywgXCJcIikucmVwbGFjZSgvXFxzXFxzKiQvLCBcIlwiKTtcblxuICAgIGlmIChTLnZhbChlbGVtKS5sZW5ndGggPD0gMCkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNoZWNrRW1haWwoZWxlbSkge1xuICAgIHZhciB0ZXh0ID0gUy52YWwoZWxlbSk7XG4gICAgaWYgKGVtYWlsUmVnRXhwLnRlc3QodGV4dCkpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBmdW5jdGlvbiBjaGVja01pbihlbGVtKSB7XG4gICAgaWYgKFMudmFsKGVsZW0pLmxlbmd0aCA8PSBTLmF0dHIoZWxlbSwgXCJkYXRhLW1pblwiKSAtIDEpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBmdW5jdGlvbiBjaGVja01heChlbGVtKSB7XG4gICAgaWYgKFMudmFsKGVsZW0pLmxlbmd0aCA+IFMuYXR0cihlbGVtLCBcImRhdGEtbWF4XCIpKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgZnVuY3Rpb24gY2hlY2tMb3dlcmNhc2UoZWxlbSkge1xuICAgIHJldHVybiAvW2Etel0vLnRlc3QoUy52YWwoZWxlbSkpO1xuICB9XG5cbiAgZnVuY3Rpb24gY2hlY2tVcHBlcmNhc2UoZWxlbSkge1xuICAgIHJldHVybiAvW0EtWl0vLnRlc3QoUy52YWwoZWxlbSkpO1xuICB9XG5cbiAgZnVuY3Rpb24gY2hlY2tOdW1iZXJFeGlzdChlbGVtKSB7XG4gICAgcmV0dXJuIC9bMC05XS8udGVzdChTLnZhbChlbGVtKSk7XG4gIH1cblxuICBmdW5jdGlvbiBjaGVja0RpZ2l0cyhlbGVtKSB7XG4gICAgcmV0dXJuIC9eXFxkKyQvLnRlc3QoUy52YWwoZWxlbSkpO1xuICB9XG5cbiAgZnVuY3Rpb24gY2hlY2tDb21wYXJlKGVsZW0sIHZhbCkge1xuICAgIHJldHVybiBTLnZhbChlbGVtKSAhPT0gdmFsO1xuICB9XG5cbiAgZnVuY3Rpb24gY2hlY2tFcXVhbChlbGVtLCB2YWwpIHtcbiAgICByZXR1cm4gUy52YWwoZWxlbSkgPT09IHZhbDtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNoZWNrQ2hlY2tib3goZWxlbSkge1xuICAgIGlmIChTLnByb3AoZWxlbSwgXCJjaGVja2VkXCIpKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBmdW5jdGlvbiBjaGVja0dyb3VwKGdyb3VwKSB7XG4gICAgbGV0IGlucHV0cyA9IFMuZmluZChncm91cCwgJ2lucHV0W3R5cGU9XCJjaGVja2JveFwiXSwgaW5wdXRbdHlwZT1cInJhZGlvXCJdJyk7XG4gICAgbGV0IHJlc3VsdHMgPSBbXTtcblxuICAgIFMuZWFjaChpbnB1dHMsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJlc3VsdHMucHVzaChTLnByb3AodGhpcywgXCJjaGVja2VkXCIpKTtcbiAgICB9KTtcblxuICAgIGlmIChyZXN1bHRzLmluZGV4T2YodHJ1ZSkgPT0gLTEpIHtcbiAgICAgIFMuYWRkQ2xhc3MoZ3JvdXAsIFwiZXJyb3JcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgIFMucmVtb3ZlQ2xhc3MoZ3JvdXAsIFwiZXJyb3JcIik7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc3VsdHMuaW5kZXhPZih0cnVlKSAhPSAtMTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNoZWNrRmlsZUV4aXN0KGVsZW0pIHtcbiAgICBpZiAoIWVsZW0uZmlsZXNBcnIpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gZWxlbS5maWxlc0Fyci5sZW5ndGggPiAwO1xuICB9XG5cbiAgZnVuY3Rpb24gc3RhcnRQcmVsb2FkKCkge1xuICAgIFMuYWRkQ2xhc3MocGFyYW1zLmZvcm1CdG4sIFwiYnRuLS1wcmVsb2FkXCIpLmF0dHIoXG4gICAgICBwYXJhbXMuZm9ybUJ0bixcbiAgICAgIFwiZGlzYWJsZWRcIixcbiAgICAgIHRydWVcbiAgICApO1xuICAgIFMuYWRkQ2xhc3MocGFyZW50LCBcImZvcm0tLXByZWxvYWRcIik7XG4gIH1cblxuICBmdW5jdGlvbiBzdG9wUHJlbG9hZCgpIHtcbiAgICBTLnJlbW92ZUNsYXNzKHBhcmFtcy5mb3JtQnRuLCBcImJ0bi0tcHJlbG9hZFwiKS5hdHRyKFxuICAgICAgcGFyYW1zLmZvcm1CdG4sXG4gICAgICBcImRpc2FibGVkXCIsXG4gICAgICBmYWxzZVxuICAgICk7XG4gICAgUy5yZW1vdmVDbGFzcyhwYXJlbnQsIFwiZm9ybS0tcHJlbG9hZFwiKTtcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgaXNWYWxpZDogc3VjY2VzVmFsdWUsXG4gICAgY2xlYXJJbnB1dHM6IGNsZWFySW5wdXRzLFxuICAgIHN0YXJ0UHJlbG9hZDogc3RhcnRQcmVsb2FkLFxuICAgIHN0b3BQcmVsb2FkOiBzdG9wUHJlbG9hZCxcbiAgICBzZXRTdWNjZXNzOiBzZXRTdWNjZXNzLFxuICB9O1xufVxuXG4vLyBBdXRvZmlsbCBmb3JtXG5pZiAoZm9ybUF1dG9maWxsKSB7XG4gIFMub24oXCIuZm9ybSBpbnB1dFwiLCBcImtleXVwXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgaWYgKGUuYWx0S2V5ID09IHRydWUgJiYgZS5rZXlDb2RlID09IDg2KSB7XG4gICAgICBsZXQgdGhpc0Zvcm0gPSBTLmNsb3Nlc3QodGhpcywgXCIuZm9ybVwiKTtcbiAgICAgIGxldCB0ZXh0SW5wcyA9IFMuZmluZCh0aGlzRm9ybSwgJ2lucHV0W3R5cGU9XCJ0ZXh0XCJdJyk7XG4gICAgICBsZXQgcGFzc0lucHMgPSBTLmZpbmQodGhpc0Zvcm0sICdpbnB1dFt0eXBlPVwicGFzc3dvcmRcIl0nKTtcbiAgICAgIGxldCBudW1iZXJJbnBzID0gUy5maW5kKFxuICAgICAgICB0aGlzRm9ybSxcbiAgICAgICAgJ2lucHV0W3R5cGU9XCJudW1iZXJcIl0sIGlucHV0W3R5cGU9XCJ0ZWxcIl0nXG4gICAgICApO1xuICAgICAgbGV0IGVtYWlsSW5wcyA9IFMuZmluZCh0aGlzRm9ybSwgJ2lucHV0W3R5cGU9XCJlbWFpbFwiXScpO1xuICAgICAgbGV0IHRleHRhcmVhID0gUy5maW5kKHRoaXNGb3JtLCBcInRleHRhcmVhXCIpO1xuICAgICAgUy5hZGRDbGFzcyh0aGlzLCBcImFjdGl2ZVwiKTtcbiAgICAgIFMuYWRkQ2xhc3ModGV4dElucHMsIFwiYWN0aXZlXCIpO1xuICAgICAgUy5hZGRDbGFzcyhwYXNzSW5wcywgXCJhY3RpdmVcIik7XG4gICAgICBTLmFkZENsYXNzKG51bWJlcklucHMsIFwiYWN0aXZlXCIpO1xuICAgICAgUy5hZGRDbGFzcyhlbWFpbElucHMsIFwiYWN0aXZlXCIpO1xuICAgICAgUy5hZGRDbGFzcyh0ZXh0YXJlYSwgXCJhY3RpdmVcIik7XG4gICAgICBTLnZhbCh0ZXh0SW5wcywgXCJBdXRvIGZpbGxlZCBpbnB1dFwiKTtcbiAgICAgIFMudmFsKHBhc3NJbnBzLCBcImF1dG9maWxsXCIpO1xuICAgICAgUy52YWwobnVtYmVySW5wcywgXCIxMjM0NTY3ODlcIik7XG4gICAgICBTLnZhbChlbWFpbElucHMsIFwiYXV0b2ZpbGxAZm9ybS5jb21cIik7XG4gICAgICBTLnZhbChcbiAgICAgICAgdGV4dGFyZWEsXG4gICAgICAgIFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gQ3JhcyB1bHRyaWNpZXMgbG9ib3J0aXMgZmVsaXMsIGV1IGZlcm1lbnR1bSB0b3J0b3IgYWxpcXVldCBldS4gTnVuYyBuZXF1ZSBuaWJoLCBhY2N1bXNhbiBub24gcGxhY2VyYXQgYXQsIGltcGVyZGlldCBuZWMgbGliZXJvLiBQcm9pbiBuZWMgbnVsbGEgZXQgcmlzdXMgYWNjdW1zYW4gdml2ZXJyYSBzZWQgZGFwaWJ1cyBkaWFtLiBVdCB2ZWwgbWF1cmlzIGNvbW1vZG8sIHBvc3VlcmUgdG9ydG9yIGlkLCBwaGFyZXRyYSBsaWJlcm8uIE51bGxhIGZhdWNpYnVzIHNlbSBldSBtYXNzYSBtYWxlc3VhZGEgYWxpcXVldC4gSW50ZWdlciB1bHRyaWNlcyBsaWJlcm8gdmVuZW5hdGlzIGVsaXQgZWdlc3RhcyBhbGlxdWFtLiBNb3JiaSB2aXZlcnJhIG5pc2wgZXJvcywgdml0YWUgZWxlaWZlbmQgZW5pbSB2dWxwdXRhdGUgYWMuIFZlc3RpYnVsdW0gdmVuZW5hdGlzIGV1IGxhY3VzIGV0IHVsdHJpY2llcy4gSW4gaGFjIGhhYml0YXNzZSBwbGF0ZWEgZGljdHVtc3QuIEV0aWFtIGp1c3RvIGlwc3VtLCB2aXZlcnJhIHF1aXMgcHJldGl1bSBhLCBncmF2aWRhIHF1aXMgbWV0dXMuXCJcbiAgICAgICk7XG4gICAgfVxuICB9KTtcbn1cblxuaWYgKCF3aW5kb3cuQXBwKSB7XG4gIHdpbmRvdy5BcHAgPSB7fTtcbiAgd2luZG93LkFwcC5WYWxpZGF0aW9uID0gVmFsaWRhdGlvbjtcbn0gZWxzZSB7XG4gIHdpbmRvdy5BcHAuVmFsaWRhdGlvbiA9IFZhbGlkYXRpb247XG59XG4iLCIvLyBKU1xuXG4oZnVuY3Rpb24gKCkge1xuICAgIGxldCBuYXYgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbmF2Jyk7XG4gICAgaWYgKCFuYXYpIHJldHVybiAvLyBFeGl0IGlmICNuYXYgZG9lc24ndCBleGlzdFxuXG4gICAgbGV0IHRvZ2dsZUJ0biA9IG5hdi5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJyNuYXZfX3RvZ2dsZScpO1xuXG4gICAgbGV0IG5hdkJHID0gbmF2LnF1ZXJ5U2VsZWN0b3IoJy5uYXZfX2JnJylcbiAgICBsZXQgbmF2T3BlbiA9IG5ldyBDdXN0b21FdmVudCgnbmF2T3BlbicpXG4gICAgbGV0IG5hdkNsb3NlID0gbmV3IEN1c3RvbUV2ZW50KCduYXZDbG9zZScpXG4gICAgbGV0IGlzT3BlbiA9IGZhbHNlO1xuXG5cbiAgICB0b2dnbGVCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0b2dnbGVOYXYpO1xuICAgIG5hdkJHLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdG9nZ2xlTmF2KTtcblxuICAgIGZ1bmN0aW9uIHRvZ2dsZU5hdigpIHtcbiAgICAgICAgaWYgKGlzT3Blbikge1xuICAgICAgICAgICAgZG9jdW1lbnQuZGlzcGF0Y2hFdmVudChuYXZDbG9zZSlcbiAgICAgICAgICAgIG5hdi5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJylcbiAgICAgICAgICAgIHRvZ2dsZUJ0bi5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJylcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmRpc3BhdGNoRXZlbnQobmF2T3BlbilcbiAgICAgICAgICAgIG5hdi5jbGFzc0xpc3QuYWRkKCdvcGVuJylcbiAgICAgICAgICAgIHRvZ2dsZUJ0bi5jbGFzc0xpc3QuYWRkKCdvcGVuJylcbiAgICAgICAgfVxuXG4gICAgICAgIGlzT3BlbiA9ICFpc09wZW5cbiAgICB9XG5cblxufSkoKSIsIi8vIENvbXBvbmVudHNcbmltcG9ydCBcIi4uL2NvbXBvbmVudHMvYnV0dG9uL2J1dHRvblwiO1xuaW1wb3J0IFwiLi4vY29tcG9uZW50cy9uYXYvbmF2XCI7XG5pbXBvcnQgXCIuLi9jb21wb25lbnRzL2hlYWRlci9oZWFkZXJcIjtcbmltcG9ydCBcIi4uL2NvbXBvbmVudHMvaWNvbi9pY29uXCI7XG5pbXBvcnQgXCIuLi9jb21wb25lbnRzL2Zvcm0vZm9ybVwiO1xuaW1wb3J0IFwiLi4vY29tcG9uZW50cy9waWN0dXJlL3BpY3R1cmVcIjtcbmltcG9ydCBcIi4uL2NvbXBvbmVudHMvc29jaWFscy9zb2NpYWxzXCI7XG4vLyBFbmQ6IENvbXBvbmVudHNcblxuLy8gUGFnZXNcbmltcG9ydCBcIi4uL3BhZ2VzL2luZGV4L2luZGV4XCI7XG4vLyBFbmQ6IFBhZ2VzXG5cbi8vIFNlY3Rpb25zXG5pbXBvcnQgXCIuLi9zZWN0aW9ucy9oZXJvL2hlcm9cIjtcblxuaW1wb3J0ICcuLi9zZWN0aW9ucy9mb290ZXIvZm9vdGVyJztcbmltcG9ydCAnLi4vc2VjdGlvbnMvZm9vdGVyL2Zvb3Rlcic7XG4vLyBFbmQ6IFNlY3Rpb25zXG5cbmxldCBib2R5ID0gUy5maXJzdChcImJvZHlcIik7XG5cbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJuYXZPcGVuXCIsIGZ1bmN0aW9uICgpIHtcbiAgUy5hZGRDbGFzcyhib2R5LCBcIm92ZXJmbG93LWhpZGRlblwiKTtcbn0pO1xuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm5hdkNsb3NlXCIsIGZ1bmN0aW9uICgpIHtcbiAgUy5yZW1vdmVDbGFzcyhib2R5LCBcIm92ZXJmbG93LWhpZGRlblwiKTtcbn0pO1xuIiwibGV0IGhlcm8gPSBTLmZpcnN0KFwiLmhlcm9cIik7XG5sZXQgaGVyb1N3aXBlclByZXZFbG0gPSBTLmZpcnN0KGhlcm8sIFwiLnN3aXBlci1idXR0b24tcHJldlwiKTtcbmxldCBoZXJvU3dpcGVyTmV4dEVsbSA9IFMuZmlyc3QoaGVybywgXCIuc3dpcGVyLWJ1dHRvbi1uZXh0XCIpO1xuXG5jb25zb2xlLmxvZyhoZXJvU3dpcGVyTmV4dEVsbSk7XG5cbmxldCBzdyA9IG5ldyBTd2lwZXIoXCIjc3dpcGVyLWhlcm9cIiwge1xuICBuYXZpZ2F0aW9uOiB7XG4gICAgbmV4dEVsOiBoZXJvU3dpcGVyTmV4dEVsbSxcbiAgICBwcmV2RWw6IGhlcm9Td2lwZXJQcmV2RWxtLFxuICB9LFxufSk7XG5cbmNvbnNvbGUubG9nKHN3KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=